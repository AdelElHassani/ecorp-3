import os, shutil, json, datetime, random

try:
    shutil.rmtree("files")
except:
    pass
os.makedirs("files/users")
os.mkdir("files/orders")

with open('users.json', 'r') as users_file:
    users = json.load(users_file)

for user in users:
    try:
        os.mkdir('files/users/{last_name}'.format(**user))
    except:
        pass
    user["dob"] = datetime.datetime.fromtimestamp(user["birthdate"]).strftime('%m/%d/%Y')
    with open('files/users/{last_name}/{first_name}'.format(**user), 'w') as f:
        f.write("""
name: {username}
fullname: {title} {first_name} {last_name}
birthdate: {dob}
adress: {location[street]} {location[postcode]} {location[city]}, {location[state]}
phone: {phone_number}
email: {email}
                """.format(**user))

with open('cart.json', 'r') as carts_file:
    carts = json.load(carts_file)

for cart in carts["carts"]:
    cart["client"] = users[random.randint(0, 200)]["username"]
    txt = \
"""
#: {id}
client: {client}
products:
""".format(**cart)
    for prod in cart["products"]:
        txt += \
"""{id} {title} {price}€ x {quantity}
""".format(**prod)
    txt += "TOTAL: {total}€".format(**cart)
    with open('files/orders/{client}-{id}'.format(**cart), 'w') as f:
        f.write(txt)